import os
import sys

#print(sys.argv[0]) # prints python_script.py
#print(sys.argv[1]) # prints .tcl file path
#print(sys.argv[2]) # prints .tcl file name

# specifying the stats.csv file path
#path = 'SUMO/Grid'
path = sys.argv[1]

# specifying the stats.csv file names
file_name = sys.argv[2]
file_name2 = sys.argv[2][:-4] + "_2.csv"
protocol = path.replace("/","").split("Stats")[-1]

#print(protocol)

# changing dir to the .tcl file
try:
    os.chdir(path)
    #print("Current working directory: {0}".format(os.getcwd()))
except FileNotFoundError:
    print("Directory: {0} does not exist.  Aborting".format(path))
    sys.exit(1)

# opening the files in READ mode
try:
    file = open(file_name, "r")
except FileNotFoundError:
    print("File: {0} not found.  Aborting".format(file_name))
    sys.exit(1)

try:
    file2 = open(file_name2, "r")
except FileNotFoundError:
    print("File: {0} not found.  Aborting".format(file_name2))
    sys.exit(1)


with file:
    with open(protocol + ".csv", "w") as temp:
        for line in file:
            temp.write(line.replace(",", " "))

with file2:
    with open(protocol + "_2.csv", "w") as temp2:
        for line in file2:
            temp2.write(line.replace(",", " "))


file.close()
