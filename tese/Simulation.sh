#!!/bin/bash

#echo "Usage:"
#echo "Script para converter simulacao sumo (file.sumocfg) para um ficheiro (file_mobility.tcl) de forma a ser usado no NS3"
#echo

SUMOCFG_FOLDER=$1
SUMOCFG_FILE=$2

#echo "$SUMOCFG_FOLDER"
#echo "$SUMOCFG_FILE"
echo

WORKDIR=$(pwd)
NS3_PATH=/home/ricardo/ns-allinone-3.34/ns-3.34

if [[ "$SUMOCFG_FOLDER" == "" ]]; then
  echo "You need to define the folder which contains the .sumocfg file."
  echo "Also, this script needs to be run as root user in .../datasets folder."
  echo
  return
fi

if [[ "$SUMOCFG_FILE" == "" ]]; then
  echo "You need to define the .sumocfg file."
  echo "Also, this script needs to be run as root user in .../datasets folder."
  echo
  return
fi

#--------------------

#First command (file.sumocfg -> file_mobility.xml)
sumo -c $WORKDIR/$SUMOCFG_FOLDER$SUMOCFG_FILE --fcd-output $WORKDIR/$SUMOCFG_FOLDER${SUMOCFG_FILE::-8}_trace.xml
echo

#--------------------

#Second command (file_trace.xml -> grid_mobility.tcl)
python3 $SUMO_HOME/tools/traceExporter.py -i $WORKDIR/$SUMOCFG_FOLDER${SUMOCFG_FILE::-8}_trace.xml --ns2mobility-out=$WORKDIR/$SUMOCFG_FOLDER${SUMOCFG_FILE::-8}_mobility.tcl
#echo "$WORKDIR/$SUMOCFG_FOLDER${SUMOCFG_FILE::-8}_mobility.tcl"
echo

#--------------------

#Script to check node number and simulation's duration
#x=($(py TclParser.py)) #Windows
x=($(python3 TclParser.py $SUMOCFG_FOLDER ${SUMOCFG_FILE::-8}_mobility.tcl)) #Linux

#echo ${x[*]}
#echo

#--------------------

#Third command (grid_mobility.tcl -> ns3 simulation) (Not Needed)
#cd $NS3_PATH
#./waf --run "scratch/ns2-mobility-trace --traceFile=$WORKDIR/$SUMOCFG_FOLDER${SUMOCFG_FILE::-8}_mobility.tcl --nodeNum=${x[3]} --duration=${x[4]} --logFile=$WORKDIR/$SUMOCFG_FOLDER${SUMOCFG_FILE::-8}_ns2-mob.log"
#cd $WORKDIR
#echo

#--------------------

#Fourth command (grid_mobility.tcl -> ns3 VANET simulation)
#Line 2387
#--traceMobility:    Enable mobility tracing [false]
#--protocol:         1=OLSR;2=AODV;3=DSDV;4=DSR [2]
#--lossModel:        1=Friis;2=ItuR1411Los;3=TwoRayGround;4=LogDistance [3]
#--fading:           0=None;1=Nakagami;(buildings=1 overrides) [0]
#--phyMode:          Wifi Phy mode [OfdmRate6MbpsBW10MHz]
#--80211Mode:        1=802.11p; 2=802.11b; 3=WAVE-PHY [1]

#--asciiTrace=1
#--routingTables=1
#--pcap=1
#--nodes=${x[3]}
#--totaltime=${x[4]}

cd $NS3_PATH

#1=OLSR - Optimized Link State Routing Protocol
./waf --run "scratch/vanet-routing-compare.cc --traceFile=$WORKDIR/$SUMOCFG_FOLDER/${SUMOCFG_FILE::-8}_mobility.tcl --nodes=${x[3]} --totaltime=${x[4]} --protocol=1 --scenario=2 --routingTables=1 --pcap=1"
mkdir $WORKDIR/$SUMOCFG_FOLDER/Stats
cp $WORKDIR/AllStats.plt $WORKDIR/$SUMOCFG_FOLDER/Stats
cp $WORKDIR/ComparisonStats.plt $WORKDIR/$SUMOCFG_FOLDER/Stats

mkdir $WORKDIR/$SUMOCFG_FOLDER/Stats/OLSR
mv  routing_table $WORKDIR/$SUMOCFG_FOLDER/Stats/OLSR/${SUMOCFG_FILE::-8}_routing_table
cd $WORKDIR/$SUMOCFG_FOLDER
mv *_mobility* $WORKDIR/$SUMOCFG_FOLDER/Stats/OLSR
mv $WORKDIR/$SUMOCFG_FOLDER/Stats/OLSR/${SUMOCFG_FILE::-8}_mobility.tcl $WORKDIR/$SUMOCFG_FOLDER
cd $NS3_PATH

#2=AODV - Ad-hoc On Demand Distance Vector
./waf --run "scratch/vanet-routing-compare.cc --traceFile=$WORKDIR/$SUMOCFG_FOLDER/${SUMOCFG_FILE::-8}_mobility.tcl --nodes=${x[3]} --totaltime=${x[4]} --protocol=2 --scenario=2 --routingTables=1 --pcap=1"
mkdir $WORKDIR/$SUMOCFG_FOLDER/Stats/AODV
mv  routing_table $WORKDIR/$SUMOCFG_FOLDER/Stats/AODV/${SUMOCFG_FILE::-8}_routing_table
cd $WORKDIR/$SUMOCFG_FOLDER
mv *_mobility* $WORKDIR/$SUMOCFG_FOLDER/Stats/AODV
mv $WORKDIR/$SUMOCFG_FOLDER/Stats/AODV/${SUMOCFG_FILE::-8}_mobility.tcl $WORKDIR/$SUMOCFG_FOLDER
cd $NS3_PATH

#3=DSDV - Destination-Sequenced Distance Vector routing
./waf --run "scratch/vanet-routing-compare.cc --traceFile=$WORKDIR/$SUMOCFG_FOLDER/${SUMOCFG_FILE::-8}_mobility.tcl --nodes=${x[3]} --totaltime=${x[4]} --protocol=3 --scenario=2 --routingTables=1 --pcap=1"
mkdir $WORKDIR/$SUMOCFG_FOLDER/Stats/DSDV
mv  routing_table $WORKDIR/$SUMOCFG_FOLDER/Stats/DSDV/${SUMOCFG_FILE::-8}_routing_table
cd $WORKDIR/$SUMOCFG_FOLDER
mv *_mobility* $WORKDIR/$SUMOCFG_FOLDER/Stats/DSDV
mv $WORKDIR/$SUMOCFG_FOLDER/Stats/DSDV/${SUMOCFG_FILE::-8}_mobility.tcl $WORKDIR/$SUMOCFG_FOLDER
cd $NS3_PATH

#4=DSR  - Dynamic Source Routing
#./waf --run "scratch/vanet-routing-compare.cc --traceFile=$WORKDIR/$SUMOCFG_FOLDER/${SUMOCFG_FILE::-8}_mobility.tcl --nodes=${x[3]} --totaltime=${x[4]} --protocol=4 --scenario=2 --routingTables=1 --pcap=1"
#mkdir $WORKDIR/$SUMOCFG_FOLDER/Stats/DSR
#mv  routing_table $WORKDIR/$SUMOCFG_FOLDER/Stats/DSR/${SUMOCFG_FILE::-8}_routing_table
#cd $WORKDIR/$SUMOCFG_FOLDER
#mv *_mobility* $WORKDIR/$SUMOCFG_FOLDER/Stats/DSR
#mv $WORKDIR/$SUMOCFG_FOLDER/Stats/DSR/${SUMOCFG_FILE::-8}_mobility.tcl $WORKDIR/$SUMOCFG_FOLDER

#--------------------

cd $WORKDIR

#python3 flowmon-parse-results.py $WORKDIR/$SUMOCFG_FOLDER/Stats/OLSR/${SUMOCFG_FILE::-8}_mobility_FlowMonitor.xml
python3 flowmon-parse-results.py $WORKDIR/$SUMOCFG_FOLDER/Stats/AODV/${SUMOCFG_FILE::-8}_mobility_FlowMonitor.xml
python3 flowmon-parse-results.py $WORKDIR/$SUMOCFG_FOLDER/Stats/DSDV/${SUMOCFG_FILE::-8}_mobility_FlowMonitor.xml
#python3 flowmon-parse-results.py $WORKDIR/$SUMOCFG_FOLDER/Stats/DSR/${SUMOCFG_FILE::-8}_mobility_FlowMonitor.xml


#python3 StatsParser.py $WORKDIR/$SUMOCFG_FOLDER/Stats/OLSR/ ${SUMOCFG_FILE::-8}_mobility_stats.csv
python3 StatsParser.py $WORKDIR/$SUMOCFG_FOLDER/Stats/AODV/ ${SUMOCFG_FILE::-8}_mobility_stats.csv
python3 StatsParser.py $WORKDIR/$SUMOCFG_FOLDER/Stats/DSDV/ ${SUMOCFG_FILE::-8}_mobility_stats.csv
#python3 StatsParser.py $WORKDIR/$SUMOCFG_FOLDER/Stats/DSR/ ${SUMOCFG_FILE::-8}_mobility_stats.csv

#--------------------

mv $WORKDIR/$SUMOCFG_FOLDER/Stats/OLSR/*OLSR* $WORKDIR/$SUMOCFG_FOLDER/Stats
mv $WORKDIR/$SUMOCFG_FOLDER/Stats/AODV/*AODV* $WORKDIR/$SUMOCFG_FOLDER/Stats
mv $WORKDIR/$SUMOCFG_FOLDER/Stats/DSDV/*DSDV* $WORKDIR/$SUMOCFG_FOLDER/Stats
#mv $WORKDIR/$SUMOCFG_FOLDER/Stats/DSR/*DSR* $WORKDIR/$SUMOCFG_FOLDER/Stats

cd $WORKDIR/$SUMOCFG_FOLDER/Stats

#gnuplot AllStats.plt 

gnuplot ComparisonStats.plt

#--------------------

cd $WORKDIR
echo
echo "Done"
