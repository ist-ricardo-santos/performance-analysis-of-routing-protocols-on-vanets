import os
import sys

print(sys.argv[0]) # prints python_script.py
print(sys.argv[1]) # prints .tcl file path
print(sys.argv[2]) # prints .tcl file name

# specifying the .tcl file path
#path = 'SUMO/Grid'
path = sys.argv[1]

# specifying the .tcl file name
#file_name = 'grid_mobility.tcl'
file_name = sys.argv[2]

# changing dir to the .tcl file
try:
    os.chdir(path)
    #print("Current working directory: {0}".format(os.getcwd()))
except FileNotFoundError:
    print("Directory: {0} does not exist.  Aborting".format(path))
    sys.exit(1)

# opening the file in READ mode
try:
    file = open(file_name, "r")
except FileNotFoundError:
    print("File: {0} not found.  Aborting".format(file_name))
    sys.exit(1)

last_node, duration = 0, 0

for line in file:

    if line.startswith("$node"):
        strnum = ""
        for char in line[7:]:
            if char == ")": break
            strnum += char

        last_node = int(strnum)

    if line.startswith("$ns"):
        strnum = ""
        for char in line[8:]:
            if char == " ": break
            strnum += char

        duration = float(strnum)


file.close()

print(last_node + 1, duration + 0.01)
