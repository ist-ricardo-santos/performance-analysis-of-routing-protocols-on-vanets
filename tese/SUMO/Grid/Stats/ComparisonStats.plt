set terminal pdf

set output "ReceiveRate_C.pdf"
set title "Receive Rate"
set xlabel "Simulation Time (Seconds)"
set ylabel "Receive Rate"
plot "AODV.csv" using 1:2 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:2 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:2 with linespoints pointinterval 10 title "DSDV"


set output "PacketsReceived_C.pdf"
set title "Packets Received"
set xlabel "Simulation Time (Seconds)"
set ylabel "PAckets Received"
plot "AODV.csv" using 1:3 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:3 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:3 with linespoints pointinterval 10 title "DSDV"


set output "WavePktsSent_C.pdf"
set title "Wave Pkts Sent"
set xlabel "Simulation Time (Seconds)"
set ylabel "Wave Pkts Sent"
plot "AODV.csv" using 1:7 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:7 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:7 with linespoints pointinterval 10 title "DSDV"


set output "WavePtksReceived_C.pdf"
set title "Wave Ptks Received"
set xlabel "Simulation Time (Seconds)"
set ylabel "Wave Ptks Received"
plot "AODV.csv" using 1:8 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:8 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:8 with linespoints pointinterval 10 title "DSDV"


set output "WavePktsPpr_C.pdf"
set title "Wave Pkts Ppr"
set xlabel "Simulation Time (Seconds)"
set ylabel "Wave Pkts Ppr"
plot "AODV.csv" using 1:9 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:9 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:9 with linespoints pointinterval 10 title "DSDV"


set output "ExpectedWavePktsReceived_C.pdf"
set title "Expected Wave Pkts Received"
set xlabel "Simulation Time (Seconds)"
set ylabel "Expected Wave Pkts Received"
plot "AODV.csv" using 1:10 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:10 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:10 with linespoints pointinterval 10 title "DSDV"


set output "ExpectedWavePktsInCoverageReceived_C.pdf"
set title "Expected Wave Pkts In Coverage Received"
set xlabel "Simulation Time (Seconds)"
set ylabel "Expected Wave Pkts In Coverage Received"
plot "AODV.csv" using 1:11 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:11 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:11 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR1_C.pdf"
set title "BSM PDR1"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR1"
plot "AODV.csv" using 1:12 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:12 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:12 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR2_C.pdf"
set title "BSM PDR2"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR2"
plot "AODV.csv" using 1:13 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:13 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:13 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR3_C.pdf"
set title "BSM PDR3"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR3"
plot "AODV.csv" using 1:14 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:14 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:14 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR4_C.pdf"
set title "BSM PDR4"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR4"
plot "AODV.csv" using 1:15 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:15 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:15 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR5_C.pdf"
set title "BSM PDR5"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR5"
plot "AODV.csv" using 1:16 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:16 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:16 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR6_C.pdf"
set title "BSM PDR6"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR6"
plot "AODV.csv" using 1:17 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:17 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:17 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR7_C.pdf"
set title "BSM PDR7"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR7"
plot "AODV.csv" using 1:18 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:18 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:18 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR8_C.pdf"
set title "BSM PDR8"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR8"
plot "AODV.csv" using 1:19 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:19 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:19 with linespoints pointinterval 10 title "DSDV"


set output "BSM_PDR9_C.pdf"
set title "BSM PDR9"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR9"
plot "AODV.csv" using 1:20 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:20 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:20 with linespoints pointinterval 10 title "DSDV",


set output "BSM_PDR10_C.pdf"
set title "BSM PDR10"
set xlabel "Simulation Time (Seconds)"
set ylabel "BSM PDR10"
plot "AODV.csv" using 1:21 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:21 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:21 with linespoints pointinterval 10 title "DSDV"


set output "MacPhyOverhead_C.pdf"
set title "Mac Phy OVerhead"
set xlabel "Simulation Time (Seconds)"
set ylabel "Mac Phy OVerhead"
plot "AODV.csv" using 1:22 with linespoints pointinterval 10 title "AODV", "OLSR.csv" using 1:22 with linespoints pointinterval 10 title "OLSR","DSDV.csv" using 1:22 with linespoints pointinterval 10 title "DSDV"


set output "AverageSpeed_C.pdf"
set title "Average Speed (m/s)"
set xlabel "Simulation Time (Seconds)"
set ylabel "Speed (m/s)"
plot "../AverageSpeed.csv" using ($0):1 with linespoints pointinterval 10 title "Average Speed"


set output "RunningVehicles_C.pdf"
set title "Running Vehicles"
set xlabel "Simulation Time (Seconds)"
set ylabel "Running Vehicles"
plot "../RunningVehicles.csv" using ($0):1 with linespoints pointinterval 10 title "Running Vehicles"
